
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/slowinger/dev/adv/realex_simple/conf/routes
// @DATE:Tue Jul 14 13:29:35 CDT 2015


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
