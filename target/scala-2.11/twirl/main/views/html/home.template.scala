
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object home_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import java.lang._
import java.util._
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import play.core.j.PlayMagicForJava._
import play.mvc._
import play.data._
import play.api.data.Field
import play.mvc.Http.Context.Implicit._

class home extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Form[Contact],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(form: Form[Contact]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.23*/("""

"""),format.raw/*3.1*/("""<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>RealEx</title>

            <!-- favicon -->
        <link rel="icon" type="image/png" href=""""),_display_(/*15.50*/routes/*15.56*/.Assets.at("images/other_images/favic.png")),format.raw/*15.99*/("""">

            <!-- Bootstrap core CSS -->
        <link href=""""),_display_(/*18.22*/routes/*18.28*/.Assets.at("bootstrap/css/bootstrap.min.css")),format.raw/*18.73*/("""" rel="stylesheet">
            <!-- Bootstrap theme -->
        <link href=""""),_display_(/*20.22*/routes/*20.28*/.Assets.at("bootstrap/css/bootstrap-theme.min.css")),format.raw/*20.79*/("""" rel="stylesheet">
            <!-- vegas bg -->
        <link href=""""),_display_(/*22.22*/routes/*22.28*/.Assets.at("javascripts/vegas/jquery.vegas.min.css")),format.raw/*22.80*/("""" rel="stylesheet">
            <!-- owl carousel css -->
        <link href=""""),_display_(/*24.22*/routes/*24.28*/.Assets.at("javascripts/owl-carousel/owl.carousel.css")),format.raw/*24.83*/("""" rel="stylesheet">
        <link href=""""),_display_(/*25.22*/routes/*25.28*/.Assets.at("javascripts/owl-carousel/owl.theme.css")),format.raw/*25.80*/("""" rel="stylesheet">
        <link href=""""),_display_(/*26.22*/routes/*26.28*/.Assets.at("javascripts/owl-carousel/owl.transitions.css")),format.raw/*26.86*/("""" rel="stylesheet">
            <!-- intro animations -->
        <link href=""""),_display_(/*28.22*/routes/*28.28*/.Assets.at("javascripts/wow/animate.css")),format.raw/*28.69*/("""" rel="stylesheet">
            <!-- font awesome -->
        <link href=""""),_display_(/*30.22*/routes/*30.28*/.Assets.at("stylesheets/font-awesome/css/font-awesome.min.css")),format.raw/*30.91*/("""" rel="stylesheet">
            <!-- lightbox -->
        <link href=""""),_display_(/*32.22*/routes/*32.28*/.Assets.at("javascripts/lightbox/css/lightbox.css")),format.raw/*32.79*/("""" rel="stylesheet">

            <!-- styles for this template -->
        <link href=""""),_display_(/*35.22*/routes/*35.28*/.Assets.at("stylesheets/styles.css")),format.raw/*35.64*/("""" rel="stylesheet">

            <!-- place your extra custom styles in this file -->
        <link href=""""),_display_(/*38.22*/routes/*38.28*/.Assets.at("stylesheets/custom.css")),format.raw/*38.64*/("""" rel="stylesheet">

            <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    </head>

    <body data-default-background-img=""""),_display_(/*47.41*/routes/*47.47*/.Assets.at("images/other_images/bg5.jpg")),format.raw/*47.88*/("""" data-overlay="true" data-overlay-opacity="0.35">

            <!-- Outer Container -->
        <div id="outer-container">

                <!-- Left Sidebar -->
            <section id="left-sidebar">

                <div class="logo">
                    <a href="#intro" class="link-scroll"><img src=""""),_display_(/*56.69*/routes/*56.75*/.Assets.at("images/other_images/logo2.png")),format.raw/*56.118*/("""" alt="RealEx"></a>
                </div><!-- .logo -->

                <!-- Menu Icon for smaller viewports -->
                <div id="mobile-menu-icon" class="visible-xs" onClick="toggle_main_menu();"><span class="glyphicon glyphicon-th"></span></div>

                <ul id="main-menu">
                    <li id="menu-item-text" class="menu-item scroll"><a href="#overview">What is RealEx?</a></li>
                    <li id="menu-item-carousel" class="menu-item scroll"><a href="#advantages">Advtanges of Our Platform</a></li>
                    <li id="menu-item-grid" class="menu-item scroll"><a href="#opportunities">Opportunities For Users</a></li>
                    <li id="menu-item-tabs" class="menu-item scroll"><a href="#about">About Us</a></li>
                    <li id="menu-item-contact" class="menu-item scroll"><a href="#contact">Contact</a></li>
                </ul><!-- #main-menu -->

            </section><!-- #left-sidebar -->
            <!-- end: Left Sidebar -->

            <section id="main-content" class="clearfix">

                <article id="intro" class="section-wrapper clearfix" data-custom-background-img=""""),_display_(/*75.99*/routes/*75.105*/.Assets.at("images/other_images/bg5.jpg")),format.raw/*75.146*/("""">
                    <div class="content-wrapper clearfix wow fadeInDown" data-wow-delay="0.3s">
                        <div class="col-sm-10 col-md-9 pull-right">

                            <section class="feature-text">
                                <h1>A Crowdsourced Commercial Real Estate Market</h1>
                                <p>RealEx intends to be the first marketplace to offer two-sided liquidity in Commercial Real Estate properties on a listing-by-listing basis.</p>
                                <p><a href="#overview" class="link-scroll btn btn-outline-inverse btn-lg">find out more</a></p>
                            </section>

                        </div><!-- .col-sm-10 -->
                    </div><!-- .content-wrapper -->
                </article><!-- .section-wrapper -->

                <article id="overview" class="section-wrapper clearfix" data-custom-background-img=""""),_display_(/*89.102*/routes/*89.108*/.Assets.at("images/other_images/bg1.jpg")),format.raw/*89.149*/("""">
                    <div class="content-wrapper clearfix">
                        <div class="col-sm-10 col-md-9 pull-right">

                            <h1 class="section-title">What is RealEx?</h1>

                            <p class="feature-paragraph">
                                RealEx is seeking to become the first
                                liquid marketplace for total or minority interests in income producing Commercial Real Estate investments.
                                In its initial phase RealEx will serve to match buyers and sellers with the information they need to make investment decisions at fair market prices.  RealEx's ultimate goal is to establish Market Makers that would ensure
                                two-sided markets on all RealEx listings, allowing for a truly liquid, constantly quoted exchange.</p>
                            <p><a href="" onclick="populate_and_open_modal(event, 'modal-content-1');" class="btn btn-outline-inverse btn-sm">read more</a></p>

                            <div class="content-to-populate-in-modal" id="modal-content-1">
                                <h1>Who Will Utilize the Platform?</h1>
                                <p>The RealEx Platform will enable all accredited investors to make and manage investments within a discreet environment. Private Real Estate sponsors will utilize the RealEx Platform to source funds,
                                    add liquidity to Platform-sourced investments, and reduce their administrative overhead.</p>
                                <h3>Who Are We Looking for?</h3>
                                <p>
                                    RealEx is currently seeking Commerical Real Estate sponsors and General Partners to join our exclusive sponsor network and change the dynamic of the Commercial Real Estate marketplace.
                                    RealEx is also interested in connecting with accredited investors and investment advisors to explore opportunities offered by the RealEx Platform.
                                    RealEx is actively seeking firms willing to be initial Market Makers on the exchange.</p>
                            </div><!-- #modal-content-1 -->

                        </div><!-- .col-sm-10 -->
                    </div><!-- .content-wrapper -->
                </article><!-- .section-wrapper -->

                <article id="advantages" class="section-wrapper clearfix" data-custom-background-img=""""),_display_(/*117.104*/routes/*117.110*/.Assets.at("images/other_images/bg6.jpg")),format.raw/*117.151*/("""">
                    <div class="content-wrapper clearfix">

                        <div id="features-carousel" class="carousel slide with-title-indicators max-height" data-height-percent="70" data-ride="carousel">

                                <!-- Indicators - slide navigation -->
                            <ol class="carousel-indicators title-indicators">
                                <li data-target="#features-carousel" data-slide-to="0" class="active">Access</li>
                                <li data-target="#features-carousel" data-slide-to="1">Liquidity</li>
                                <li data-target="#features-carousel" data-slide-to="2">Equity</li>
                                <li data-target="#features-carousel" data-slide-to="3">Cost Savings</li>
                            </ol>

                                <!-- Wrapper for slides -->
                            <div class="carousel-inner">

                                <div class="item active">
                                    <div class="carousel-text-content">
                                          <span class="icon fa fa-globe fa-5x icon"></span>
                                        <h2 class="title">Access for Investors</h2>
                                        <p>Participate in Commercial Real Estate investments from our network of high-quality sponsors operating throughout the country</p>
                                        <p><a href="" onclick="populate_and_open_modal(event, 'modal-content-2');" class="btn btn-outline-inverse btn-sm">read more</a></p>

                                        <div class="content-to-populate-in-modal" id="modal-content-2">
                                            <h1>Exclusive Sponsor Network</h1>
                                            <p><img data-img-src=""""),_display_(/*142.68*/routes/*142.74*/.Assets.at("images/other_images/transp-image7.png")),format.raw/*142.125*/("""" class="lazy rounded_border hover_effect pull-left" alt="Lorem Ipsum">RealEx has worked tirelessly to source and secure exclusive listings from a diverse network of proven Commercial Real Estate sponsors. All listings on the RealEx platform will represent a diverse group of income producing investments.</p>
                                        </div><!-- #modal-content-2 -->
                                    </div>
                                </div><!-- .item -->

                                <div class="item">
                                    <div class="carousel-text-content">
                                       <span class="icon fa fa-tint fa-5x icon"></span>
                                        <h2 class="title">Liquidity</h2>
                                        <p>Secondary market will attempt to bring liquidity to a fragmented marketplace and may reduce investment risk.</p>
                                        <p><a href="" onclick="populate_and_open_modal(event, 'modal-content-3');" class="btn btn-outline-inverse btn-sm">read more</a></p>

                                        <div class="content-to-populate-in-modal" id="modal-content-3">
                                            <h1>Liquidity</h1>
                                            <p><img data-img-src=""""),_display_(/*156.68*/routes/*156.74*/.Assets.at("images/other_images/transp-image2.png")),format.raw/*156.125*/("""" class="lazy rounded_border hover_effect pull-left" alt="Liquidity">Exit investments after set holding period through anonymous transactions with other users or Market Makers on the platform.RealEx's platform bring technology to the process of  transferring interests and ensuring tax and other documents are delivered automatically.  </p>
                                        </div><!-- #modal-content-3 -->
                                    </div>
                                </div><!-- .item -->

                                <div class="item">
                                    <div class="carousel-text-content">
                                         <span class="icon fa fa-usd fa-5x icon"></span>
                                        <h2 class="title">Own the Exchange</h2>
                                        <p>Opportunity for sponsors to earn equity in RealEx through continued platform participation</p>
                                        <p><a href="" onclick="populate_and_open_modal(event, 'modal-content-4');" class="btn btn-outline-inverse btn-sm">read more</a></p>
                                        <div class="content-to-populate-in-modal" id="modal-content-4">
                                            <h1>Good for Us, Good for You</h1>
                                            <p><img data-img-src=""""),_display_(/*169.68*/routes/*169.74*/.Assets.at("images/other_images/transp-image1.png")),format.raw/*169.125*/("""" class="lazy rounded_border hover_effect pull-left" alt="Lorem Ipsum">Join the other sponsors on the RealEx platform earning equity in RealEx as our platform grows.</p>
                                        </div><!-- #modal-content-4 -->
                                    </div>
                                </div><!-- .item -->

                                <div class="item">
                                    <div class="carousel-text-content">
                                        <span class="icon fa fa-university fa-5x icon"></span>
                                        <h2 class="title">Cost Savings to Sponsors</h2>
                                        <p>Reduce administrative overhead through automated management of partnership interest transfers and automated investment performance reporting.</p>
                                        <p>Reduce your cost of equity by raising funds through our large and diverse capital pool</p>
                                        <div class="content-to-populate-in-modal" id="modal-content-5">
                                        </div><!-- #modal-content-5 -->
                                    </div>
                                </div><!-- .item -->

                            </div><!-- .carousel-inner -->

                            <!-- Controls -->
                            <a class="left carousel-control" href="#features-carousel" data-slide="prev"></a>
                            <a class="right carousel-control" href="#features-carousel" data-slide="next"></a>

                        </div><!-- #about-carousel -->

                    </div><!-- .content-wrapper -->
                </article><!-- .section-wrapper -->

                <article id="opportunities" class="section-wrapper clearfix" data-custom-background-img=""""),_display_(/*196.107*/routes/*196.113*/.Assets.at("images/other_images/bg2.jpg")),format.raw/*196.154*/("""">
                    <div class="content-wrapper clearfix">
                        <div class="col-sm-11 col-md-10 pull-right">

                            <h1 class="section-title">Opportunity</h1>

                                <!-- grid -->
                            <section class="grid row clearfix clearfix-for-2cols">

                                    <!-- grid item -->
                                <div class="grid-item col-md-6">
                                    <div class="item-content clearfix">
                                        <span class="icon fa fa-building"></span>
                                        <div class="text-content">
                                            <h5>Sponsors</h5>
                                            <p>RealEx provides <em>access</em> to a larger, more diverse capital pool with the added <em>efficiency</em> and <em>control</em> of a
                                            web-based platform plus the <em>opportunity</em> for lower costs of capital.</p>
                                        </div>
                                    </div><!-- end: .item-content -->
                                </div><!-- end: .grid-item -->

                                <!-- grid item -->
                                <div class="grid-item col-md-6">
                                    <div class="item-content clearfix">
                                        <span class="icon fa fa-usd"></span>
                                        <div class="text-content">
                                            <h5>Investment Advisors</h5>
                                            <p>RealEx provides <em>access</em> to our exclusive network of Commerical Real Estate sponsors offering a <em>diverse</em> range of
                                            investments from around the country.  <em>Manage</em> clients' Commercial Real Estate investment like never before with our intuitive investment dashboard.
                                            Find <em>liquidty</em> when clients' needs or risk tolerance changes.</p>
                                        </div>
                                    </div><!-- end: .item-content -->
                                </div><!-- end: .grid-item -->

                                <!-- grid item -->
                                <div class="grid-item col-md-6">
                                    <div class="item-content clearfix">
                                        <span class="icon fa fa-certificate"></span>
                                        <div class="text-content">
                                            <h5>Accredited Investors</h5>
                                            <p><em>Make</em> Commercial Real Estate investments like never before with opportunities in a broad range of cash flowing assets.
                                            <em>Manage</em> these investments on our web-based platform.  <em>Create</em> the first liquid secondary market by trading shares with other RealEx users.</p>
                                        </div>
                                    </div><!-- end: .item-content -->
                                </div><!-- end: .grid-item -->

                                <!-- grid item -->
                                <div class="grid-item col-md-6">
                                    <div class="item-content clearfix">
                                        <span class="icon fa fa-users"></span>
                                        <div class="text-content">
                                            <h5>Non-Accredited Investors</h5>
                                            <p>In later iterations of the RealEx platform, we hope to add the opportunity for non-accredited investors to particpate in investments on the platform.</p>
                                        </div>
                                    </div><!-- end: .item-content -->
                                </div><!-- end: .grid-item -->

                            </section><!-- end: grid -->

                        </div><!-- .col-sm-11 -->
                    </div><!-- .content-wrapper -->
                </article><!-- .section-wrapper -->



                <article id="about" class="section-wrapper clearfix" data-custom-background-img=""""),_display_(/*261.99*/routes/*261.105*/.Assets.at("images/other_images/bg7.jpg")),format.raw/*261.146*/("""">
                    <div class="content-wrapper mid-vertical-positioning clearfix">
                        <div class="col-sm-10 col-md-9 pull-right">

                            <h1 class="section-title">About Us</h1>

                            <div class="tabpanel styled-tabs uniform-height" role="tabpanel">

                                    <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#simon-holstein" aria-controls="tabs-tab1" role="tab" data-toggle="tab"><span>Simon Holstein</span></a></li>
                                    <li role="presentation"><a href="#stephen-lowinger" aria-controls="tabs-tab2" role="tab" data-toggle="tab"><span>Stephen Lowinger</span></a></li>
                                </ul>

                                    <!-- Tab panes -->
                                <div class="tab-content">

                                    <div role="tabpanel" class="tab-pane fade in active" id="simon-holstein">
                                        <img src=""""),_display_(/*279.52*/routes/*279.58*/.Assets.at("images/other_images/simon.png")),format.raw/*279.101*/("""" class="pull-right hidden-xs">
                                        <h4>Simon Holstein</h4>
                                        <h6>Founder & CEO</h6>
                                        <p>A lifelong follower of financial markets and exposed to Commercial Real Estate since a young age, Simon naturally pondered the means of integrating the two. Simon attended the University of Illinois at Urbana-Champaign where he majored in History. He worked for Wells Fargo as an analyst in the Wholesale Banking unit focused on Commercial Real Estate. Prior to focusing on RealEx full time, Simon worked at PEAK6,
                                            a proprietary trading firm in Chicago.
                                            He is an MBA candidate at the University of Chicago, Booth School of Business. </p>
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="stephen-lowinger">
                                        <img src=""""),_display_(/*288.52*/routes/*288.58*/.Assets.at("images/other_images/stephen.png")),format.raw/*288.103*/("""" class="pull-right hidden-xs">
                                        <h4>Stephen Lowinger</h4>
                                        <h6>Cofounder & CTO</h6>
                                        <p>As CTO, Stephen leads software development for the RealEx platform.  He is passionate about designing and implementing solutions for real world problems.  Stephen is currently pursuing a master’s degree in computer science from the University of Chicago.  Previously, Stephen maintained and developed monitoring solutions for trading systems at Peak6 Investments LP.
                                            Additionally, he holds a bachelor’s degree in Mechanical Engineering from Lafayette College.</p>
                                    </div>
                                </div><!-- .tab-content -->

                            </div><!-- .tabpanel -->

                        </div><!-- .col-sm-10 -->
                    </div><!-- .content-wrapper -->
                </article><!-- .section-wrapper -->

                <article id="contact" class="section-wrapper clearfix" data-custom-background-img=""""),_display_(/*302.101*/routes/*302.107*/.Assets.at("images/other_images/bg4.jpg")),format.raw/*302.148*/("""">
                    <div class="content-wrapper clearfix">

                        <h1 class="section-title">Contact</h1>
                            <!-- CONTACT DETAILS -->
                        <div class="contact-details col-sm-5 col-md-3">
                            <p>222,<br/>W. Merchandise Mart Plaza,<br/>12th Floor<br/>Chicago, IL 60654</p>
                            <p><a href="mailto:realex.exchange@gmail..com">Email Us</a></p>
                        </div>
                            <!-- END: CONTACT DETAILS -->

                            <!-- CONTACT FORM -->
                        <div class="col-sm-7 col-md-9">
                                <!-- IMPORTANT: change the email address at the top of the assets/php/mail.php file to the email address that you want this form to send to -->
                            <form class="form-style validate-form clearfix" action=""""),_display_(/*316.86*/routes/*316.92*/.Application.sendEmail()),format.raw/*316.116*/("""" method="POST" role="form">

                                    <!-- form left col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        """),_display_(/*321.42*/helper/*321.48*/.inputText(form("name"),'class -> "text-field form-control validate-field required", '_id -> "form-name", 'type -> "text", 'placeholder -> "Full Name", 'name -> "name",'_label -> null)),format.raw/*321.232*/("""
                                        """),format.raw/*322.201*/("""
                                    """),format.raw/*323.37*/("""</div>
                                    <div class="form-group">
                                        """),_display_(/*325.42*/helper/*325.48*/.inputText(form("email"),'class -> "text-field form-control validate-field required", '_id -> "form-name", 'type -> "email", 'placeholder -> "Email Address", 'name -> "email",'_label -> null)),format.raw/*325.239*/("""
                                        """),format.raw/*326.207*/("""
                                    """),format.raw/*327.37*/("""</div>
                                    <div class="form-group">
                                        """),_display_(/*329.42*/helper/*329.48*/.inputText(form("phone"),'class -> "text-field form-control validate-field phone", '_id -> "form-contact-number", 'type -> "tel", 'placeholder -> "Contact Number", 'name -> "contact",'_label -> null)),format.raw/*329.247*/("""
                                        """),format.raw/*330.221*/("""
                                    """),format.raw/*331.37*/("""</div>

                                </div><!-- end: form left col -->

                                <!-- form right col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <textarea placeholder="Message..." class="form-control validate-field required" name="message"></textarea>
                                    </div>
                                    <div class="form-group">

                                        <button type="submit" class="btn btn-sm btn-outline-inverse">Submit</button>
                                    </div>
                                    <div class="form-group form-general-error-container"></div>
                                </div><!-- end: form right col -->

                            </form>
                        </div><!-- end: CONTACT FORM -->

                    </div><!-- .content-wrapper -->
                </article><!-- .section-wrapper -->

            </section><!-- #main-content -->

            <!-- Footer -->
            <section id="footer">

                    <!-- Go to Top -->
                <div id="go-to-top" onclick="scroll_to_top();"><span class="icon glyphicon glyphicon-chevron-up"></span></div>

                <ul class="social-icons">
                    <li><a href="https://www.linkedin.com/profile/view?id=232979315&trk=hp-identity-name" target="_blank" title="LinkedIn"><img src=""""),_display_(/*362.151*/routes/*362.157*/.Assets.at("images/theme_images/social_icons/linkedin.png")),format.raw/*362.216*/("""" alt="LinkedIn"></a></li>
                    <li><a href="https://twitter.com/RealEx_Exchange" target="_blank" title="Twitter"><img src=""""),_display_(/*363.114*/routes/*363.120*/.Assets.at("images/theme_images/social_icons/twitter.png")),format.raw/*363.178*/("""" alt="Twitter"></a></li>
                    <li><a href=http://www.1871.com target="_blank"> <img src=""""),_display_(/*364.81*/routes/*364.87*/.Assets.at("images/other_images/1871.png")),format.raw/*364.129*/(""""></a></li>
                    <!-- copyright text -->
                <div class="footer-text-line">&copy; 2015 RealEx</div>
            </section>
                <!-- end: Footer -->

        </div><!-- #outer-container -->
        <!-- end: Outer Container -->

        <!-- Modal -->
        <!-- DO NOT MOVE, EDIT OR REMOVE - this is needed in order for popup content to be populated in it -->
        <div class="modal fade" id="common-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="modal-body clearfix">
          </div><!-- .modal-body -->
                </div><!-- .modal-content -->
            </div><!-- .modal-dialog -->
        </div><!-- .modal -->

        <!-- Javascripts
    ================================================== -->

        <!-- Jquery and Bootstrap JS -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src=""""),_display_(/*390.64*/routes/*390.70*/.Assets.at("javascripts/jquery-1.11.2.min.js")),format.raw/*390.116*/(""""><\/script>')</script>
        <script src=""""),_display_(/*391.23*/routes/*391.29*/.Assets.at("bootstrap/js/bootstrap.min.js")),format.raw/*391.72*/(""""></script>

            <!-- Easing - for transitions and effects -->
        <script src=""""),_display_(/*394.23*/routes/*394.29*/.Assets.at("javascripts/jquery.easing.1.3.js")),format.raw/*394.75*/(""""></script>

            <!-- background image strech script -->
        <script src=""""),_display_(/*397.23*/routes/*397.29*/.Assets.at("javascripts/vegas/jquery.vegas.min.js")),format.raw/*397.80*/(""""></script>

            <!-- detect mobile browsers -->
        <script src=""""),_display_(/*400.23*/routes/*400.29*/.Assets.at("javascripts/detectmobilebrowser.js")),format.raw/*400.77*/(""""></script>

            <!-- detect scrolling -->
        <script src=""""),_display_(/*403.23*/routes/*403.29*/.Assets.at("javascripts/jquery.scrollstop.min.js")),format.raw/*403.79*/(""""></script>

            <!-- owl carousel js -->
        <script src=""""),_display_(/*406.23*/routes/*406.29*/.Assets.at("javascripts/owl-carousel/owl.carousel.min.js")),format.raw/*406.87*/(""""></script>

            <!-- lightbox js -->
        <script src=""""),_display_(/*409.23*/routes/*409.29*/.Assets.at("javascripts/lightbox/js/lightbox.min.js")),format.raw/*409.82*/(""""></script>

            <!-- intro animations -->
        <script src=""""),_display_(/*412.23*/routes/*412.29*/.Assets.at("javascripts/wow/wow.min.js")),format.raw/*412.69*/(""""></script>

            <!-- responsive videos -->
        <script src=""""),_display_(/*415.23*/routes/*415.29*/.Assets.at("javascripts/jquery.fitvids.js")),format.raw/*415.72*/(""""></script>

            <!-- Custom functions for this theme -->
        <script src=""""),_display_(/*418.23*/routes/*418.29*/.Assets.at("javascripts/functions.js")),format.raw/*418.67*/(""""></script>
        <script src=""""),_display_(/*419.23*/routes/*419.29*/.Assets.at("javascripts/initialise-functions.js")),format.raw/*419.78*/(""""></script>

            <!-- IE9 form fields placeholder fix -->
            <!--[if lt IE 9]>
    <script>contact_form_IE9_placeholder_fix();</script>
    <![endif]-->

    </body>
</html>"""))
      }
    }
  }

  def render(form:Form[Contact]): play.twirl.api.HtmlFormat.Appendable = apply(form)

  def f:((Form[Contact]) => play.twirl.api.HtmlFormat.Appendable) = (form) => apply(form)

  def ref: this.type = this

}


}

/**/
object home extends home_Scope0.home
              /*
                  -- GENERATED --
                  DATE: Tue Jul 14 13:29:37 CDT 2015
                  SOURCE: /Users/slowinger/dev/adv/realex_simple/app/views/home.scala.html
                  HASH: f4f9c45aea9be734c7dfae7357c56620c34c3c16
                  MATRIX: 750->1|866->22|894->24|1332->435|1347->441|1411->484|1503->549|1518->555|1584->600|1689->678|1704->684|1776->735|1874->806|1889->812|1962->864|2068->943|2083->949|2159->1004|2227->1045|2242->1051|2315->1103|2383->1144|2398->1150|2477->1208|2583->1287|2598->1293|2660->1334|2762->1409|2777->1415|2861->1478|2959->1549|2974->1555|3046->1606|3161->1694|3176->1700|3233->1736|3367->1843|3382->1849|3439->1885|3859->2278|3874->2284|3936->2325|4270->2632|4285->2638|4350->2681|5538->3842|5554->3848|5617->3889|6561->4805|6577->4811|6640->4852|9169->7352|9186->7358|9250->7399|11120->9241|11136->9247|11210->9298|12564->10624|12580->10630|12654->10681|14044->12043|14060->12049|14134->12100|15999->13936|16016->13942|16080->13983|20489->18364|20506->18370|20570->18411|21734->19547|21750->19553|21816->19596|22869->20621|22885->20627|22953->20672|24109->21799|24126->21805|24190->21846|25126->22755|25142->22761|25189->22785|25463->23031|25479->23037|25686->23221|25757->23422|25823->23459|25960->23568|25976->23574|26190->23765|26261->23972|26327->24009|26464->24118|26480->24124|26702->24323|26773->24544|26839->24581|28358->26071|28375->26077|28457->26136|28626->26276|28643->26282|28724->26340|28858->26446|28874->26452|28939->26494|30157->27684|30173->27690|30242->27736|30316->27782|30332->27788|30397->27831|30518->27924|30534->27930|30602->27976|30717->28063|30733->28069|30806->28120|30913->28199|30929->28205|30999->28253|31100->28326|31116->28332|31188->28382|31288->28454|31304->28460|31384->28518|31480->28586|31496->28592|31571->28645|31672->28718|31688->28724|31750->28764|31852->28838|31868->28844|31933->28887|32049->28975|32065->28981|32125->29019|32187->29053|32203->29059|32274->29108
                  LINES: 27->1|32->1|34->3|46->15|46->15|46->15|49->18|49->18|49->18|51->20|51->20|51->20|53->22|53->22|53->22|55->24|55->24|55->24|56->25|56->25|56->25|57->26|57->26|57->26|59->28|59->28|59->28|61->30|61->30|61->30|63->32|63->32|63->32|66->35|66->35|66->35|69->38|69->38|69->38|78->47|78->47|78->47|87->56|87->56|87->56|106->75|106->75|106->75|120->89|120->89|120->89|148->117|148->117|148->117|173->142|173->142|173->142|187->156|187->156|187->156|200->169|200->169|200->169|227->196|227->196|227->196|292->261|292->261|292->261|310->279|310->279|310->279|319->288|319->288|319->288|333->302|333->302|333->302|347->316|347->316|347->316|352->321|352->321|352->321|353->322|354->323|356->325|356->325|356->325|357->326|358->327|360->329|360->329|360->329|361->330|362->331|393->362|393->362|393->362|394->363|394->363|394->363|395->364|395->364|395->364|421->390|421->390|421->390|422->391|422->391|422->391|425->394|425->394|425->394|428->397|428->397|428->397|431->400|431->400|431->400|434->403|434->403|434->403|437->406|437->406|437->406|440->409|440->409|440->409|443->412|443->412|443->412|446->415|446->415|446->415|449->418|449->418|449->418|450->419|450->419|450->419
                  -- GENERATED --
              */
          