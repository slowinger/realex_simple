package controllers;

import com.google.inject.Inject;
import models.Contact;
import play.data.Form;
import play.mvc.*;
import views.html.*;
import play.libs.mailer.*;
import play.libs.mailer.MailerClient;

import javax.security.auth.login.Configuration;
import java.util.HashMap;
import java.util.Map;


public class Application extends Controller {

    public static final Form<Contact> contactForm = Form.form(Contact.class);
    private final MailerClient mailer;

    @Inject
    public Application(MailerClient mailer) {
        this.mailer = mailer;
    }

    public Result index() {
        return ok(home.render(contactForm));
    }




        public Result sendEmail() {
            //Extract data from the form
           // Contact contact = contactForm.get();
            Form<Contact> newQuestion = contactForm.bindFromRequest();

            Email email = new Email();
            email.setSubject("Contact Form: " + newQuestion.get().name);
            //email.setSubject("Contact Form: ");
            email.setFrom(newQuestion.get().email);
            //email.setFrom("realex.exchange@gmail.com");
            email.addTo("realex.exchange@gmail.com");
            // sends text, HTML or both...
            email.setBodyText("phone: " + newQuestion.get().phone + "\nemail: " + newQuestion.get().email + "\nmessage: " + newQuestion.get().message);
            mailer.send(email);
            System.out.println("sent");

            return redirect("/");
        }


}


